'use strict';

module.exports = exports = {};

exports.nickCommand = function(msg, socket){
  socket.username = msg.split(' ').slice(1).join(' ').trim();
  socket.write(`Nickisi on nyt ${socket.username}\n`);
};

exports.dmCommand = function(msg, socket, array){
  let toUsername =  msg.split(' ')[1];
  let content = msg.split(' ').slice(2).join(' ').trim();

  array.forEach((s) => {
    if (s.username === toUsername){
      s.write(`${socket.username}: ${content}`);
    }
  });
};

exports.allCommand = function(msg, socket, array){
  console.log('line 22', msg);
  array.forEach((s) => {
    s.write(`${socket.username}: ${msg.split(' ').slice(1).join(' ').trim()}\n`);
  });
};

exports.usersCommand = function(msg, socket, array){
  let users = array.map((element) => {
    return element.username;
  });
  socket.write(`Paikalla olevat käyttäjät:\n${users}.\nViestittääksesi käyttäjälle "/dm <käyttäjä> <viestisi>."\n`);
};