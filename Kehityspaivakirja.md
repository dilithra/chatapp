# Web-ohjelmointi, LTD7003, syksy 2018
## Kehittäjäblogi

## Sisältö
1. [Presidentit 1](#presidentit1)
2. [Simple chat server](#simplechatserver)
## Tehtävä: Presidentit 1 <a name="presidentit1"></a>
#### Ohjelmien asennus

Ohjelmien asentaminen linkattuja ohjeita noudattaen, kesti ajallisesti noin tunnin. 

#### Tehtävien tekeminen

Aikaa käytetty noin vartti.

## Tehtävä: Simple chat server <a name="simplechatserver"></a>

Aihealue oli itselle täysin tuntematon joten tämä tehtävä vaati kattavaa perehtymistä.
Aikaa tehtävän opiskeluun kului noin 8 tuntia, ja toteuttamiseen noin 2 tuntia.

Kohtien 1-3 toteutus on sisällytetty palautettuun koodiin ja linkkeihin.

Osion 4 suorittamisessa oli onglemia, joihin en tahtonut löytää ratkaisua pikaisella googlaamisella.
Onneksi kurssin ***slack*** kanavalta sai hyvin vastauksia esitettyyn kysymykseen, ja tehtävä ratkesi noin tunnin ajassa.

Hakiessani mallia tehtävään 5 löysin mielestäni varsin hyvän ratkaisun.
https://github.com/alexyoung/ircd.js

Katsotaan kerkeänkö tehdä yksinkertaistetun version palautusaikaan mennessä...


Muina huomioina markdown vaikuttaa erinomailselta ratkaisulta. Hieman kuin **LaTeX** light.