'use strict';


require('dotenv').config();
//node modules
const net = require('net');
const server = net.createServer();

//app modules
const msgParser = require('./lib/parse-message.js');

let sockets = [];

server.on('connection', function(socket){
  console.log('clientti yhdistetty');

  socket.username = `user_${Math.floor(Math.random() * 100)}`;
  sockets.push(socket);

  socket.on('data', function(buffer){
    let message = buffer.toString();

    if(message.startsWith('/nick')){
      return msgParser.nickCommand(message, socket);
    }

    if(message.startsWith('/dm')){
      return msgParser.dmCommand(message, socket, sockets);
    }
    if(message.startsWith('/all')){
      return msgParser.allCommand(message, socket, sockets);
    }
    if(message.startsWith('/users')){
      return msgParser.usersCommand(message, socket, sockets);
    }
  });

  socket.on('close', function(){
    console.log('clientti poistui');
    sockets.forEach( (client, index) => {
      if (client === socket){
        sockets.splice(index, 1);
      }
    });
  });
});

server.listen(3000, function(){
  console.log('Serveri toiminnassa portissa: ', 3000);
});
